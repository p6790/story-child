<?php

get_header();

// Variables for member data.
$studio_name   = get_the_author_meta( '_wc_memberships_profile_field_studio_name' );
$headshot_id   = get_the_author_meta( '_wc_memberships_profile_field_headshot' );
$website_url   = get_the_author_meta( '_wc_memberships_profile_field_website_link' );
$instagram_url = get_the_author_meta( '_wc_memberships_profile_field_instagram_link' );
$member_bio    = get_the_author_meta( '_wc_memberships_profile_field_short_bio' );

?>


<div class="content" style="background-color:#fff; min-height:5em;">
	<div class="member">
		<h1><?php the_author_meta( 'first_name' ); ?> <?php the_author_meta( 'last_name' ); ?></h1>
		<div class="member-bio-headshot">
			<?php
				echo wp_get_attachment_image( $headshot_id, 'large', null, array( 'class' => 'thumb' ) );
			?>
		</div>

		<div class="member-info">
			<p><span><strong>Studio Name:</strong></span> <span><?php echo $studio_name; ?></span></p>
			<p><span><strong>Website:</strong></span> <span><a href="<?php echo $website_url; ?>"><?php echo $website_url; ?></a></span></p>
			<p><span><strong>Instagram Link:</strong></span> <span><a href="<?php echo $instagram_url; ?>"><?php echo $instagram_url; ?></a></span></p>
			<div style="display:flex; justify-content:start;">
				<p><strong>Specialties</strong></p>
				<ul>
					<?php
					$specialties = get_the_author_meta( '_wc_memberships_profile_field_specialty' );
					foreach ( $specialties as $specialty ) {
						echo '<li>';
						echo $specialty;
						echo '</li>';
					};
					?>
				</ul>
			</div>	

		</div>

		<div class="member-bio">
			<h2>Bio</h2>
			<?php echo $member_bio; ?>
		</div>

		<div class="member-gallery">
			<h2>Gallery</h2>
		</div>

	</div>

</div>

<?php
get_footer();
